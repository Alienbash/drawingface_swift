//
//  FacialExpressionModel.swift
//  DrawingFace
//
//  Created by Julian Manke on 14.07.17.
//  Copyright © 2017 Julian Manke. All rights reserved.
//

import Foundation

struct FacialExpression {
    enum Eyes: Int {
        case open
        case closed
        case squinting
    }
    
    enum Mouth: Int {
        case frown
        case smirk
        case neutral
        case grin
        case smile
        
        var sadder: Mouth {
            return Mouth(rawValue: rawValue - 1) ?? .frown
        }
        var happier: Mouth {
            return Mouth(rawValue: rawValue + 1) ?? .smile
        }
    }
    
    let eyes: Eyes
    let mouth: Mouth
}
