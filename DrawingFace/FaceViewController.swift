//
//  ViewController.swift
//  DrawingFace
//
//  Created by Julian Manke on 13.07.17.
//  Copyright © 2017 Julian Manke. All rights reserved.
//

import UIKit

class FaceViewController: UIViewController {
    
    @IBOutlet weak var testMe: FaceView! { // Property observation is used here as well, bcoz swift takes a little time to make this connection. If model updates, UI needs to know --> Here: didSet only gets called one time, when iOS sets the connection
        didSet {
            let handler = #selector(FaceView.changeScale(byReactingTo:))
            let pinchRecognizer = UIPinchGestureRecognizer(target: testMe, action: handler) // every time a gesture gets recognzed, the pinch recognizer is sending a message (handler) to the target (faceView)
            testMe.addGestureRecognizer(pinchRecognizer)
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.toggleEyes(byReactingTo:)))
            tapRecognizer.numberOfTapsRequired = 1
            testMe.addGestureRecognizer(tapRecognizer)
            
            updateUI()
        }
    }
    
    /// tab handler that opens/closes the eyes and changed this in the model, or more precisely: Change the instance of the model inside the VC, responsible for data. That's why, this handler is NOT defined inside the view, but the VC
    func toggleEyes(byReactingTo tapRecognizer: UITapGestureRecognizer) {
        if tapRecognizer.state == .ended {
            let eyes: FacialExpression.Eyes = (expression.eyes == .closed) ? .open : .closed
            expression = FacialExpression(eyes: eyes, mouth: expression.mouth)
        }
    }
    

    
    // initializing model with actual (self chosen) values
    var expression = FacialExpression(eyes: .open, mouth: .neutral) { // needs to be called every time the model changes --> use property observing
        didSet {
            updateUI()
        }
    }
    
    // translate (abstract) model to UI language
    private func updateUI() { //make model match the UI
        switch expression.eyes {
        case .open:
            testMe?.eyesOpen = true // optional chaining is only used for the rare case that "var expression" is being set before outlet connection to "testMe" axists --> then testMe is nil --> crash
        case .closed:
            testMe?.eyesOpen = false
        case .squinting:
            testMe?.eyesOpen = false // just doing the best we can to match what we can actually do
        }
        testMe?.mouthCurvature = mouthCurvatures[expression.mouth] ?? 0.0 // default value of 0.0, if expression is not given
    }
    
    // same thing as above: Create a relationship between (abstract) model values and actual view values
    private let mouthCurvatures = [FacialExpression.Mouth.grin: 0.5, .frown: -1.0, .smile: 1.0, .neutral: 0.0, .smirk: -0.5]

}

