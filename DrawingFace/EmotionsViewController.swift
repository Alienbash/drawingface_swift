//
//  EmotionsViewController.swift
//  DrawingFace
//
//  Created by Julian Manke on 20.07.17.
//  Copyright © 2017 Julian Manke. All rights reserved.
//

import UIKit

class EmotionsViewController: UIViewController {
    
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var destinationVC = segue.destination
        if let navigationController = destinationVC as? UINavigationController {
            destinationVC = navigationController.visibleViewController ?? destinationVC
        }
        if let faceVC = destinationVC as? FaceViewController,
            let identifier = segue.identifier,
            let expression = emotionalFaces[identifier] {
            faceVC.expression = expression
            
            faceVC.navigationItem.title = (sender as? UIButton)?.currentTitle //Note: "sender" is of type "Any?", which means it doen not knoe any methods --> downcast it to UIButton, in ordet to access its methods.
        }
    }
    
    private let emotionalFaces: Dictionary<String, FacialExpression> = [
        "sad" : FacialExpression(eyes: .closed, mouth: .frown),
        "happy" : FacialExpression(eyes: .open, mouth: .smile),
        "worried" : FacialExpression(eyes: .open, mouth: .smirk)
    ]
    
}
