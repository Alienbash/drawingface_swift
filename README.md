

### What is this repository for? ###

- Simple iOS App, written in Swift 3, that draws a face inside an UIView and recognizes tab- as well as pinch gestures
- Project follows simple MVC pattern

### How do I get set up? ###

- To set it up, just download the XCode project and run it on any device with iOS > 10.0. 
- No external dependencies needed.


### Who do I talk to? ###

For any questions and/or suggestions, feel free to text me at: Julian.Manke91@gmail.com